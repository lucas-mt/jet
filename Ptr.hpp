#pragma once

#include <memory>

namespace jet
{
template<class T>
using ptr = std::shared_ptr<T>;
}
