#pragma once

#include "JoinExpTree.hpp"
#include "Optional.hpp"

namespace jet
{
/*!
 * \class JETFunction
 *
 * \brief Class defining a case-based function over a JoinExpressionTree.
 */
template<class T>
class JETFunction : public JETVisitor
{
protected:

  /*
   * Ideally, derived classes should never access _result outside the visitor methods.
   * However, I don't think there's a way to enforce that.
   */
  optional<T> _result;

public:

  /*!
   * \brief Case for the empty node. Result should be stored in \a _result member.
   */
  void forEmptyNode(const EmptyNode& emptyNode) = 0;

  /*!
   * \brief Case for the leaf node. Result should be stored in \a _result member.
   */
  void forLeafNode(const LeafNode& leafNode) = 0;

  /*!
   * \brief Case for the join node. Result should be stored in \a _result member.
   */
  void forJoinNode(const JoinNode& joinNode) = 0;

  /*!
   * \brief Case for the projection node. Result should be stored in \a _result member.
   */
  void forProjNode(const ProjNode& projNode) = 0;

  /*!
   * \brief
   */
  void forListNode(const ListNode& listNode) = 0;
  
  /*!
   * \brief Applies function to given node and returns result.
   */
  T eval(const JoinExpTree& jet)
  {
    jet.accept(*this);

    return *_result;
  }

  /* Virtual destructor */
  virtual ~JETFunction() {}
};
}
