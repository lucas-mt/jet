#include "PrimalGraph.hpp"

namespace jet
{
PrimalGraph::PrimalGraph(const AttrSet& allAttrs,
			 const std::vector<AttrSet>& hyperedges)
{
    for (Attr attr : allAttrs)
    {
	_neighbors[attr]; // create empty list of neighbors
    }

    for (const AttrSet& relatedAttrs : hyperedges)
    {
	for (auto it = relatedAttrs.begin(); it != relatedAttrs.end(); it++)
	{
	    Attr a = *it;

	    for (auto it2 = std::next(it); it2 != relatedAttrs.end(); it2++)
	    {
		Attr b = *it2;

		_neighbors[a].insert(b);
		_neighbors[b].insert(a);
	    }
	}
    }
}

size_t PrimalGraph::vertexCount() const
{
    return _neighbors.size();
}
}
