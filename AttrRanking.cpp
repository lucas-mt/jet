#include "AttrRanking.hpp"
#include "PrimalGraph.hpp"
#include "InOrderSet.hpp"
#include "PriorityQueue.hpp"

#include <stdexcept>

namespace jet
{
AttrPosition::AttrPosition(Attr attr, size_t rank)
    : _attr(attr)
    , _rank(rank)
{}

Attr AttrPosition::attr() const
{
    return _attr;
}

size_t AttrPosition::rank() const
{
    return _rank;
}

template<class Iterator>
void AttrRanking::init(Iterator first, Iterator last)
{
    for (auto it = first; it != last; it++)
    {
	if (_rank.find(*it) != _rank.end())
	{
	    throw std::invalid_argument(
		"Tried initializing AttrRanking with repeated elements.");
	}
	else
	{
	    _rank[*it] = _rankedAttrs.size();
	    _rankedAttrs.push_back(*it);
	}
    }
}
  
template<class Iterator>
AttrRanking::AttrRanking(Iterator first, Iterator last)
{
  init(first, last);
}

AttrRanking::AttrRanking(const std::vector<Attr>& attrs)
{
  init(attrs.begin(), attrs.end());
}

AttrRanking::AttrRanking(const AttrSet& attrs)
{
    for (Attr attr : attrs)
    {
	_rank[attr] = _rankedAttrs.size();
	_rankedAttrs.push_back(attr);
    }
}

AttrRanking::AttrRanking(size_t n)
{
  for (size_t i = 0; i < n; i++)
  {
    Attr attr(i);
    _rank[attr] = _rankedAttrs.size();
    _rankedAttrs.push_back(attr);
  }
}
  
size_t AttrRanking::length() const
{
    return _rankedAttrs.size();
}

Attr AttrRanking::operator[](size_t i) const
{
    return _rankedAttrs[i];
}

AttrRanking AttrRanking::reverse() const
{
    return AttrRanking(_rankedAttrs.rbegin(), _rankedAttrs.rend());
}

bool AttrRanking::lessThan(Attr a1, Attr a2) const
{
    return _rank.at(a1) < _rank.at(a2);
}

optional<AttrPosition> AttrRanking::minOf(const AttrSet& attrs) const
{
    optional<AttrPosition> min = nullopt;

    for (Attr attr : attrs)
    {
	auto it = _rank.find(attr);

	if (it != _rank.end() && (!min || min->rank() > it->second))
	{
	    min = AttrPosition(attr, it->second);
	}
    }

    return min;
}

optional<AttrPosition> AttrRanking::maxOf(const AttrSet& attrs) const
{
    optional<AttrPosition> max = nullopt;

    for (Attr attr : attrs)
    {
	auto it = _rank.find(attr);

	if (it != _rank.end() && (!max || max->rank() < it->second))
	{
	    max = AttrPosition(attr, it->second);
	}
    }

    return max;
}

AttrRanking AttrRanking::MCS(const AttrSet& allAttrs,
			     const std::vector<AttrSet>& hyperedges)
{
    PrimalGraph graph(allAttrs, hyperedges);

    data::InOrderSet<Attr> ranking;
    ranking.reserve(graph.vertexCount());

    std::vector<data::WithPriority<Attr, size_t>> weightedAttr;

    graph.forEachVertex([&weightedAttr] (Attr a)
			{
			    weightedAttr.emplace_back(a, 0);
			});

    data::PriorityQueue<Attr, size_t> byWeight(weightedAttr.begin(),
					       weightedAttr.end());

    auto increment = [] (size_t priority) { return priority + 1; };

    while (true)
    {
	optional<Attr> top = byWeight.pop();

	if (!top)
	    break;

	Attr maxWeightAttr = *top;

	if (!ranking.contains(maxWeightAttr))
	{
	    ranking.insert(maxWeightAttr);

	    graph.forEachNeighbor(maxWeightAttr,
				  [&byWeight, &increment] (Attr neighbor)
				  {
				      byWeight.updatePriority(neighbor,
							      increment);
				  });
	}
    }

    return AttrRanking(ranking.rbegin(), ranking.rend());
}
}
