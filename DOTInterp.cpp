#include "DOTInterp.hpp"

#include <sstream>

namespace jet
{
DOTInterp::DOTInterp(std::string filename,
		     std::string emptyLabel,
		     std::function<std::string(size_t)> leafLabel,
		     std::string joinLabel,
		     std::function<std::string(AttrSet)> projLabel)
  : _out(filename)
  , _emptyLabel(std::move(emptyLabel))
  , _leafLabel(leafLabel)
  , _joinLabel(std::move(joinLabel))
  , _projLabel(projLabel)
  , _nameId(0)
{}

std::string DOTInterp::newName()
{
  std::stringstream ss;
  
  ss << "a" << (_nameId++);

  return ss.str();
}

std::string DOTInterp::emptyInterp()
{
  std::string name = newName();

  _out << name << " [label=\"" << _emptyLabel << "\"];" << std::endl;

  return name;
}

std::string DOTInterp::leafInterp(size_t id)
{
  std::string name = newName();

  _out << name << " [label=\"" << _leafLabel(id) << "\"];" << std::endl;

  return name;
}

std::string DOTInterp::joinInterp(const std::vector<std::string>& args)
{
  std::string name = newName();

  _out << name << "[label=\"" << _joinLabel << "\"];" << std::endl;

  for (const std::string& childName : args)
  {
    _out << name << " -- " << childName << ";" << std::endl;
  }

  return name;
}

std::string DOTInterp::projInterp(const AttrSet& projAttrs,
				  const std::string& arg)
{
  std::string name = newName();

  _out << name
	    << " [label=\"" << _projLabel(projAttrs) << "\"];" << std::endl;

  _out << name << " -- " << arg << ";" << std::endl;

  return name;
}

void DOTInterp::dumpDot(const JoinExpTree& tree, const std::string& graphName)
{
  _out << "graph " << graphName << " {" << std::endl;

  this->eval(tree);

  _out << "}" << std::endl;
}
}
