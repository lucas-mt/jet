#include "ClusterAndJoin.hpp"

namespace jet
{
ClusterAndJoin::ClusterAndJoin(std::unique_ptr<Clustering> clustering,
			       std::unique_ptr<JoinAlgorithm> joinAlgorithm)
    : _clustering(std::move(clustering))
    , _joinAlgorithm(std::move(joinAlgorithm))
{}

JoinExpTree ClusterAndJoin::run(const std::vector<JoinExpTree>& trees) const
{
    std::vector<JoinExpTree> clusters = _clustering->apply(trees);

    return _joinAlgorithm->run(clusters);
}
}
