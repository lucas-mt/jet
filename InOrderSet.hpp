#pragma once

#include <vector>
#include <unordered_map>

namespace jet
{
namespace data
{
	/*!
	 * \class InOrderSet
	 *
	 * \brief Data structure that stores elements in the order they were inserted.
	 *
	 */
	template<class T>
	class InOrderSet
	{
		std::unordered_map<T, size_t> _index;
		std::vector<T> _elems;

	public:

		/*!
		 * \brief Reserves space for \a n elements in the set.
		 */
		void reserve(size_t n)
		{
			_elems.reserve(n);
		}

		/*!
		 * \brief Inserts \a elem in the set.
		 */
		void insert(const T& elem)
		{
			_index[elem] = _elems.size();
			_elems.push_back(elem);
		}

		/*!
		 * \brief Returns true if the set contains the given element.
		 */
		bool contains(const T& elem) const
		{
			return _index.find(elem) != _index.end();
		}

		/* Iterators */
		typedef typename std::vector<T>::const_iterator const_iterator;
		typedef typename std::vector<T>::const_reverse_iterator const_reverse_iterator;

		inline const_iterator begin() const { return _elems.begin(); }
		inline const_iterator end() const { return _elems.end(); }
		inline const_reverse_iterator rbegin() const { return _elems.rbegin(); }
		inline const_reverse_iterator rend() const { return _elems.rend(); }

		/*!
		 * \brief Returns an iterator to the requested element, if it is in the set.
		 *
		 * Otherwise, returns an iterator to the end of the container.
		 */
		const_iterator find(const T& elem) const
		{
			auto it = _index.find(elem);

			if (it == _index.end())
			{
				return _elems.end();
			}
			else
			{
				return _elems.begin() + it->second;
			}
		}
	};
}
}
