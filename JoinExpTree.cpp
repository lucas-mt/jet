#include "JoinExpTree.hpp"

namespace jet
{
/* JoinExpTree */

JoinExpTree::JoinExpTree(ptr<JETNode> node) : _node(node) {}

JoinExpTree JoinExpTree::empty()
{
	return JoinExpTree(std::make_shared<EmptyNode>());
}

JoinExpTree JoinExpTree::leaf(size_t id, AttrSet label)
{
	return JoinExpTree(std::make_shared<LeafNode>(id, label));
}

JoinExpTree JoinExpTree::join(std::vector<JoinExpTree> children)
{
	size_t size = children.size();

	if (size == 0)
	{
		return JoinExpTree(std::make_shared<EmptyNode>());
	}
	else if (size == 1)
	{
		return children[0];
	}
	else
	{
		return JoinExpTree(std::make_shared<JoinNode>(std::move(children)));
	}
}

JoinExpTree JoinExpTree::proj(AttrSet toProject) const
{
  return JoinExpTree(std::make_shared<ProjNode>(toProject, *this));
}

JoinExpTree JoinExpTree::list(std::vector<LinearStep> steps)
{
  return JoinExpTree(std::make_shared<ListNode>(std::move(steps)));
}
  
JoinExpTree JoinExpTree::operator*(JoinExpTree other) const
{
	std::vector<JoinExpTree> children = { *this, other };

	return JoinExpTree(std::make_shared<JoinNode>(std::move(children)));
}

AttrSet JoinExpTree::label() const
{
	return _node->label();
}

void JoinExpTree::accept(JETVisitor& visitor) const
{
	_node->accept(visitor);
}

/* JETNode and subclasses */

JETNode::JETNode(AttrSet label) : _label(label) {}

AttrSet JETNode::label() const
{
	return _label;
}

EmptyNode::EmptyNode() : JETNode({}) {}

void EmptyNode::accept(JETVisitor& visitor) const
{
	visitor.forEmptyNode(*this);
}

LeafNode::LeafNode(size_t id, AttrSet label) : JETNode(label), _id(id) {}

size_t LeafNode::id() const
{
    return _id;
}

void LeafNode::accept(JETVisitor& visitor) const
{
	visitor.forLeafNode(*this);
}

JoinNode::JoinNode(std::vector<JoinExpTree> children)
	: JETNode({})
	, _children(std::move(children))
{
	std::vector<AttrSet> labels;

	for (JoinExpTree child : _children)
	{
		labels.push_back(child.label());
	}

	_label = AttrSet::bigUnion(labels);
}

size_t JoinNode::childCount() const
{
	return _children.size();
}

JoinExpTree JoinNode::getChild(size_t i) const
{
	return _children[i];
}

void JoinNode::accept(JETVisitor& visitor) const
{
	visitor.forJoinNode(*this);
}

ProjNode::ProjNode(AttrSet projAttrs, JoinExpTree child)
	: JETNode(child.label().differenceWith(projAttrs))
	, _projAttrs(projAttrs)
	, _child(child)
{}

AttrSet ProjNode::projectedAttrs() const
{
	return _projAttrs;
}

JoinExpTree ProjNode::child() const
{
	return _child;
}

void ProjNode::accept(JETVisitor& visitor) const
{
	visitor.forProjNode(*this);
}

LinearStep::LinearStep(JoinExpTree tree, AttrSet attrs)
  : _tree(std::move(tree))
  , _attrs(std::move(attrs))
{}

JoinExpTree LinearStep::toJoin() const
{
  return _tree;
}

AttrSet LinearStep::toProject() const
{
  return _attrs;
}

AttrSet ListNode::remainingAttrs(const std::vector<LinearStep>& steps)
{
  std::vector<AttrSet> attrSets;

  for (const LinearStep& step : steps)
  {
    AttrSet remaining = step.toJoin().label().differenceWith(step.toProject());

    attrSets.push_back(remaining);
  }

  return AttrSet::bigUnion(attrSets);
}
  
ListNode::ListNode(std::vector<LinearStep> steps)
  : JETNode(remainingAttrs(steps))
  , _steps(std::move(steps))
{}

void ListNode::accept(JETVisitor& visitor) const
{
  visitor.forListNode(*this);
}
}
