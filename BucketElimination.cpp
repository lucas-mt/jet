#include "BucketElimination.hpp"

namespace jet
{
RankBasedClustering::RankBasedClustering(AttrRanking ranking)
	: _ranking(std::move(ranking))
{}

std::vector<JoinExpTree> RankBasedClustering::apply(
	const std::vector<JoinExpTree>& trees) const
{
  size_t n = _ranking.length() + 1;
  std::vector<std::vector<JoinExpTree>> buckets(n);

  for (const auto& tree : trees)
  {
    /* Cluster by minimum attribute according to the ranking */
    optional<AttrPosition> min = _ranking.minOf(tree.label());

    if (!min)
      {
	buckets.back().push_back(tree);
      }
    else
      {
	buckets[min->rank()].push_back(tree);
      }
  }

  std::vector<JoinExpTree> clusters;
  clusters.reserve(n);

  for (int i = 0; i < n; i++)
  {
    clusters.push_back(JoinExpTree::join(buckets[i]));
  }

  return clusters;
}

AttrRanking RankBasedClustering::ranking() const
{
  return _ranking;
}

BucketElimination::BucketElimination(RankBasedClustering clustering)
	: _clustering(std::move(clustering))
{}

BucketElimination::BucketElimination(AttrRanking ranking)
	: _clustering(std::move(ranking))
{}

JoinExpTree BucketElimination::run(const std::vector<JoinExpTree>& trees) const
{
  std::vector<JoinExpTree> clusters = _clustering.apply(trees);
  AttrRanking ranking = _clustering.ranking();
  size_t last = clusters.size() - 1;

  for (size_t i = 0; i < last; i++)
  {
    AttrSet attr({ ranking[i] });

    clusters[i] = clusters[i].proj(attr);

    optional<AttrPosition> newMin = ranking.minOf(clusters[i].label());

    size_t j = !newMin ? last : newMin->rank();

    clusters[j] = JoinExpTree::join({ clusters[j], clusters[i] });
  }

  return clusters[last];
}
}
