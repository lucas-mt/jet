#pragma once

#include "JETInterp.hpp"

#include <fstream>
#include <functional>
#include <vector>

namespace jet
{
/*!
 * \class DOTInterp
 *
 * \brief 
 */
class DOTInterp : public JETInterp<std::string>
{
  std::ofstream _out;
  std::string _emptyLabel;
  std::function<std::string(size_t)> _leafLabel;
  std::string _joinLabel;
  std::function<std::string(AttrSet)> _projLabel;

  size_t _nameId;

  std::string newName();
  
public:

  DOTInterp(std::string filename,
	    std::string emptyLabel,
	    std::function<std::string(size_t)> leafLabels,
	    std::string joinLabel,
	    std::function<std::string(AttrSet)> projLabel);
  
  /*!
   * \brief Interpretation for the empty node.
   */
  std::string emptyInterp();

  /*!
   * \brief Interpretation for the leaf node.
   */
  std::string leafInterp(size_t id);

  /*!
   * \brief Interpretation for the join node.
   */
  std::string joinInterp(const std::vector<std::string>& args);

  /*!
   * \brief Interpretation for the projection node.
   */
  std::string projInterp(const AttrSet& projAttrs, const std::string& arg);

  /*!
   * \brief
   */
  void dumpDot(const JoinExpTree& tree, const std::string& graphName);
};
}
