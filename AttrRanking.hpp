#pragma once

#include "Attr.hpp"

#include <vector>
#include <unordered_map>

namespace jet
{
/*!
 * \class AttrPosition
 *
 * \brief Record indicating the position of an attribute in a ranking.
 */
class AttrPosition
{
  Attr _attr;
  size_t _rank;

public:

  /*!
   * \brief Constructs a position given an attribute and its rank.
   */
  AttrPosition(Attr attr, size_t rank);

  /*!
   * \brief Returns the attribute at this position.
   */
  Attr attr() const;

  /*!
   * \brief Returns the rank this position represents.
   */
  size_t rank() const;
};

/*!
 * \class AttrRanking
 *
 * \brief Ranking defining a linear order over a set of attributes.
 */
class AttrRanking
{
  // Invariants: never null, no repeated elements
  std::vector<Attr> _rankedAttrs;

  // Invariant: never null, _rank[a] = i iff _rankedAttrs[i] = a
  std::unordered_map<Attr, size_t> _rank;

  template<class Iterator>
  void init(Iterator first, Iterator last);

public:

  /*!
   * \brief Constructs a ranking following the order of the given range.
   *
   * \a first and \a last must represent a range of attributes [first, last)
   * in ascending rank, with no repeated elements.
   */
  template<class Iterator>
  AttrRanking(Iterator first, Iterator last);

  /*!
   * \brief Constructs a ranking following the order of the given vector.
   *
   * \a attrs must have no repeated elements.
   */
  AttrRanking(const std::vector<Attr>& attrs);
  
  /*!
   * \brief Ranks attributes according to their natural order in the set.
   */
  AttrRanking(const AttrSet& attrs);

  /*!
   * \brief Constructs a ranking of attributes with ids [0, n-1], in order.
   */
  AttrRanking(size_t n);

  /*!
   * \brief Returns the number of attributes in the ranking.
   */
  size_t length() const;

  /*!
   * \brief Returns the attribute at the i-th position of the ranking.
   *
   * 0 is the position of the lowest-ranked attribute, and length() - 1 is
   * the position of the highest-ranked attribute.
   */
  Attr operator[](size_t i) const;

  /*!
   * \brief Returns a ranking over the same attributes in the reverse order.
   */
  AttrRanking reverse() const;

  /*!
   * \brief Tests whether \a a1 is lower in the ranking than \a a2.
   */
  bool lessThan(Attr a1, Attr a2) const;

  /*!
   * \brief Tests whether \a a1 is higher in the ranking than \a a2.
   */
  bool greaterThan(Attr a1, Attr a2) const;

  /*!
   * \brief Returns lowest-ranked attribute in the given set.
   */
  optional<AttrPosition> minOf(const AttrSet& attrs) const;

  /*!
   * \brief Returns highest-ranked attribute in the given set.
   */
  optional<AttrPosition> maxOf(const AttrSet& attrs) const;

  /* Iterators */
  typedef typename std::vector<Attr>::const_iterator const_iterator;

  inline const_iterator begin() const { return _rankedAttrs.begin(); }
  inline const_iterator end() const { return _rankedAttrs.end(); }

  /*!
   * \brief Computes a ranking using Maximum Cardinality Search (MCS).
   *
   * MCS computes a ranking in the following way:
   * 1. Construct the primal graph from the set of attributes.
   * 2. At every step, pick the next highest-ranked attribute as the one with
   * the most neighbors already ranked.
   */
  static AttrRanking MCS(const AttrSet& allAttrs,
                         const std::vector<AttrSet>& hyperedges);
};
}
