#include "Attr.hpp"

#include <sstream>

namespace jet
{
Attr::Attr(size_t id) : _id(id) {}

size_t Attr::id() const
{
    return _id;
}

bool Attr::operator==(Attr other) const
{
    return id() == other.id();
}

bool Attr::operator!=(Attr other) const
{
    return id() != other.id();
}

bool Attr::operator<(Attr other) const
{
    return id() < other.id();
}

bool Attr::operator<=(Attr other) const
{
    return id() <= other.id();
}

bool Attr::operator>(Attr other) const
{
    return id() > other.id();
}

bool Attr::operator>=(Attr other) const
{
    return id() >= other.id();
}

std::string Attr::toStringWith(std::string name) const
{
    std::stringstream sstream;

    sstream << name << id();

    return sstream.str();
}

AttrSet::AttrSet(ptr<std::vector<Attr>> attrs)
    : _attrs(std::move(attrs))
{}

AttrSet::AttrSet()
{
    _attrs = std::make_shared<std::vector<Attr>>();
}

AttrSet::AttrSet(std::vector<Attr> attrs)
{
    _attrs = std::make_shared<std::vector<Attr>>(attrs);

    std::sort(_attrs->begin(), _attrs->end());

    // Remove duplicates
    std::vector<Attr>::iterator newEnd =
	std::unique(_attrs->begin(), _attrs->end());
    _attrs->resize(std::distance(_attrs->begin(), newEnd), Attr(0));
}

AttrSet AttrSet::singleton(Attr attr)
{
    return AttrSet(std::vector<Attr>(1, attr));
}

size_t AttrSet::size() const
{
    return _attrs->size();
}

AttrSet AttrSet::unionWith(const AttrSet& other) const
{
    ptr<std::vector<Attr>> attrs = std::make_shared<std::vector<Attr>>();

    attrs->reserve(size() + other.size());

    std::set_union(_attrs->begin(), _attrs->end(),
		   other._attrs->begin(), other._attrs->end(),
		   std::back_inserter(*attrs));

    return AttrSet(attrs);
}

AttrSet AttrSet::intersectWith(const AttrSet& other) const
{
    ptr<std::vector<Attr>> attrs = std::make_shared<std::vector<Attr>>();

    attrs->reserve(size() + other.size());

    std::set_intersection(_attrs->begin(), _attrs->end(),
			  other._attrs->begin(), other._attrs->end(),
			  std::back_inserter(*attrs));

    return AttrSet(attrs);
}

AttrSet AttrSet::differenceWith(const AttrSet& other) const
{
    ptr<std::vector<Attr>> attrs = std::make_shared<std::vector<Attr>>();

    attrs->reserve(size() + other.size());

    std::set_difference(_attrs->begin(), _attrs->end(),
			other._attrs->begin(), other._attrs->end(),
			std::back_inserter(*attrs));

    return AttrSet(attrs);
}

AttrSet AttrSet::bigUnion(const std::vector<AttrSet>& args)
{
    AttrSet result;

    for (AttrSet arg : args)
    {
	result = result.unionWith(arg);
    }

    return result;
}

bool AttrSet::hasElem(Attr attr) const
{
    return std::binary_search(_attrs->begin(), _attrs->end(), attr);
}

bool AttrSet::subsetOf(const AttrSet& set) const
{
    for (Attr attr : *_attrs)
    {
	AttrSet::const_iterator it = set.begin();

	while (it != set.end() && *it != attr)
	{
	    it++;
	}

	if (it == set.end())
	{
	    return false;
	}
    }

    return true;
}

bool AttrSet::operator==(const AttrSet& other) const
{
    AttrSet::const_iterator it1 = begin();
    AttrSet::const_iterator it2 = other.begin();

    while (it1 != end() && it2 != other.end() && *it1 == *it2)
    {
	it1++;
	it2++;
    }

    return it1 == end() && it2 == other.end();
}

bool AttrSet::operator!=(const AttrSet& other) const
{
    return !(*this == other);
}

optional<Attr> AttrSet::maxElem() const
{
    if (_attrs->empty())
    {
	return nullopt;
    }
    else
    {
	return _attrs->back();
    }
}

std::string AttrSet::toStringWith(std::string name) const
{
    std::stringstream sstream;

    sstream << "{ ";

    const_iterator it = begin();

    while (it != end())
    {
	sstream << it->toStringWith(name);
	it++;

	if (it != end())
	{
	    sstream << ", ";
	}
    }

    sstream << " }";

    return sstream.str();
}
}
