#pragma once

#include "Attr.hpp"

#include <unordered_set>
#include <unordered_map>

namespace jet
{
/*!
 * \class PrimalGraph
 *
 * \brief Graph representation of a collection of attribute sets.
 *
 * In a primal graph, vertices corresponds to attributes and there is an
 * edge between two attributes if and only if they occur in the same set.
 */
class PrimalGraph
{
    std::unordered_map<Attr, std::unordered_set<Attr>> _neighbors;

public:

    /*!
     * \brief Constructs the primal graph for a set of attributes.
     */
    PrimalGraph(const AttrSet& allAttrs,
		const std::vector<AttrSet>& hyperedges);

    /*!
     * \brief Returns the number of vertices in the graph.
     */
    size_t vertexCount() const;

    /*!
     * \brief Executes the given action for each vertex.
     *
     * The action is assumed to define operator() with an attribute as argument.
     */
    template<class F>
    void forEachVertex(const F& action) const
    {
	for (const auto& entry : _neighbors)
	{
	    action(entry.first);
	}

    }

    /*!
     * \brief Executes the given action for each neighbor of the given vertex.
     *
     * The action is assumed to define operator() with an attribute as argument.
     */
    template<class F>
    void forEachNeighbor(Attr attr, const F& action) const
    {
	for (Attr neighbor : _neighbors.at(attr))
	{
	    action(neighbor);
	}
    }
};
}
