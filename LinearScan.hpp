#pragma once

#include "JoinExpTree.hpp"
#include "JoinAlgorithm.hpp"
#include "Optional.hpp"

namespace jet
{
/*!
 * \class LinearScheduler
 *
 * \brief Object that chooses a scheduling for joining trees linearly.
 */
class LinearScheduler
{
public:

	/*!
	 * \brief Returns the next step in the join.
	 */
	virtual optional<LinearStep> nextStep() = 0;

	/*!
	 * \brief Virtual destructor.
	 */
	virtual ~LinearScheduler() = 0;
};

/*!
 * \class LinearScan
 *
 * \brief Algorithm for joining trees in a linear fashion.
 */
class LinearScan : public JoinAlgorithm
{
	AttrSet _projectedAttrs;

protected:

	/*!
	 * \brief Factory method for constructing a linear scheduler.
	 */
	virtual ptr<LinearScheduler> makeScheduler(const AttrSet& projectedAttrs,
	                                           const std::vector<JoinExpTree>& trees) const = 0;

public:

	/*!
	 * \brief Constructs a linear scan algorithm given the attributes to project
	 */
	LinearScan(AttrSet projectedAttrs);

	/*!
	 * \brief Joins the trees using the linear algorithm.
	 */
	JoinExpTree run(const std::vector<JoinExpTree>& trees) const;

	/*!
	 * \brief Virtual destructor.
	 */
	virtual ~LinearScan() = 0;
};
}
