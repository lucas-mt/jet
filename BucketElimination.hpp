#pragma once

#include "Ptr.hpp"
#include "JoinExpTree.hpp"
#include "Clustering.hpp"
#include "AttrRanking.hpp"
#include "JoinAlgorithm.hpp"

#include <vector>

namespace jet
{
/*!
 * \class RankBasedClustering
 *
 * \brief Clustering of join-expression trees by the highest-ranked attribute.
 */
class RankBasedClustering : public Clustering
{
	AttrRanking _ranking;

public:

	/*!
	 * \brief Constructs a clustering based on the given ranking.
	 *
	 * The ranking need not include all attributes. During clustering, trees
	 * with no ranked attributes will be clustered together.
	 */
	RankBasedClustering(AttrRanking ranking);

	/*!
	 * \brief Clusters the given list of join-expression trees.
	 *
	 * Depending on the ranking used for clustering, the returned vector is as
	 * follows:
	 * - The size of the vector is n, where (n - 1) is the maximum rank.
	 * - For i between 0 and n - 1, the cluster in position i is the join of
	 * all trees whose minimum rank is i.
	 * - For i = n, the cluster in position i is the join of all trees that
	 * contain no ranked elements.
	 */
	std::vector<JoinExpTree> apply(const std::vector<JoinExpTree>& trees) const;

	/*!
	 * \brief Returns the attribute ranking used for clustering.
	 */
	AttrRanking ranking() const;
};

/*!
 * \class BucketElimination
 *
 * \brief Join algorithm based on rank-based clustering.
 */
class BucketElimination : public JoinAlgorithm
{
	RankBasedClustering _clustering;

public:

	/*!
	 * \brief Constructs the algorithm given the clustering to be used.
	 */
	BucketElimination(RankBasedClustering clustering);

	/*!
	 * \brief Constructs the algorithm given the ranking to be used.
	 */
	BucketElimination(AttrRanking ranking);

	/*!
	 * \brief Joins the given trees according to the algorithm.
	 */
	JoinExpTree run(const std::vector<JoinExpTree>& trees) const;
};
}
