#pragma once

#include "JoinExpTree.hpp"

namespace jet
{
/*!
 * \class JoinAlgorithm
 *
 * \brief Algorithm for joining a list of join-expression trees.
 */
class JoinAlgorithm
{
public:

	/*!
	 * \brief Joins the given trees according to the algorithm.
	 */
	virtual JoinExpTree run(const std::vector<JoinExpTree>& trees) const = 0;

	/*!
	 * \brief Virtual destructor.
	 */
	virtual ~JoinAlgorithm() {}
};
}
