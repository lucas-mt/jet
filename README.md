# JET

JET (short for *Join-Expression Tree*) is designed to be an application-independent library for problems that follow a join/projection model. Problems of this type consist of:

- A set of *objects*, each with a set of associated *attributes*;
- A *join* operation that combines two or more objects;
- A *projection* operation that abstracts away attributes from an object;
- A set of attributes to be projected.

The goal is to produce an object that is equivalent to the join of all given objects with the given attributes projected out. The problem is to choose the best way to perform joins and projections in order to compute the final result efficiently.

This problem reoccurs in a number of different applications, including databases, model checking, and simulation of physical systems. The JET library abstracts away the details regarding what the objects, attributes, and operations are, so that it can be integrated into any possible application that follows a join/projection model. This abstraction is achieved through the concept of *join-expression trees*.

## Join-Expression Trees

A *join-expression tree* is an abstract representation of an expression that performs joins and projections over a set of objects. The JET library constructs such a tree from an abstract representation of the objects to be joined and attributes to be projected. This tree can then be evaluated by the calling application in order to obtain the final result.

Every node in a join-expression tree is labeled by a set of attributes. These attributes are represented by unique identifiers encoded as unsigned integers. There are four types of nodes:

- **Empty nodes** have no children and an empty label. They are used to represent a join over zero elements.
- **Leaf nodes** represent the objects given as input to the join/projection problem, and are labeled by their associated set of attributes. Leaf nodes have no children and also hold an identifier, which can be used to map them to the object they represent.
- **Join nodes** have two or more children, and represent the join of their children. The label of a join node is always the union of its children's labels.
- **Projection nodes** have a single child and a set of projected attributes. They represent the result of projecting out the given attributes from the child. The label of a projection node is always the label of the child with the projected attributes removed.

In a valid join-expression tree, for every attribute, the nodes with that attribute in their labels form a subtree. This means that an attribute cannot be projected until all subtrees containing this attribute have been joined. Below we show an example of how to use the JET library to construct a join-expression tree from a join/projection problem and evaluate this tree on the input objects.

## Example

In this example we use an application that needs to perform existential quantification over a conjunction of boolean formulas. This is a use case that appears for example in the context of model checking. The formulas are represented by Binary Decision Diagrams (BDDs) using the CUDD library. In the join/projection model, we have:

- The objects are the boolean formulas (represented as BDDs), with the associated attributes being the variables in the support of the formula;
- The join operation is conjunction;
- The projection operation is existential quantification;
- The projected attributes are the existentially-quantified variables.

### Constructing the leaf nodes

We start by constructing one leaf node for each BDD. The leaf node for a certain BDD will be labeled by a set of attributes representing the variables in that BDD's support. We use the variable indices as the attribute identifiers. The leaf nodes also need identifiers, which later when we interpret the join-expression tree will be used to find the BDD corresponding to a node. For that we simply use the index of the BDD in the vector.

	std::vector<BDD> bdds;

	// Initialize vector of BDDs here

	std::vector<jet::JoinExpTree> leaves(bdds.size());

	for (size_t i = 0; i < bdds.size(); i++)
	{
		std::vector<jet::Attr> attrs;

		for (unsigned int var : bdds[i].SupportIndices())
		{
			attrs.push_back(jet::Attr(var));
		}

		jet::AttrSet label(attrs);

		leaves[i] = jet::JoinExpTree::leaf(i, label);
	}

### Clustering

Before we run a join algorithm to construct a full join-expression tree, we have the option of performing clustering on the leaf nodes. This operation partitions the list of nodes, and the elements within a partition are joined together to form a cluster. In this example, we use rank-based clustering, which clusters trees based on a ranking of attributes. In this case, we use the maximum-cardinality search as the ranking method. All clustering algorithms inherit from the Clustering class.

	std::vector<jet::AttrSet> labels(leaves.size());

	for (size_t i = 0; i < leaves.size(); i++)
	{
		labels[i] = leaves[i].label();
	}

	jet::AttrRanking ranking = jet::AttrRanking::MCS(labels);

	jet::RankBasedClustering clustering(ranking);

	std::vector<jet::JoinExpTree> clusters = clustering.apply(leaves);

Clustering is an optional step, but in some cases it can improve performance by grouping related leaves. One may also apply different types of clustering in sequence. Additionally, some join algorithms, such as bucket elimination, already apply clustering as a preprocessing step.

### Joining the clusters

To construct a full join-expression tree from the leaf nodes or clusters, we use a join algorithm. All join algorithms inherit from the JoinAlgorithm class. In this case, we use a greedy linear scan algorithm, which is initialized with the set of attributes that must be projected out. The algorithms will generally try to project out attributes as soon as possible.

	jet::AttrSet toProject;

	jet::GreedyScan joinAlgorithm(toProject);

	jet::JoinExpTree tree = joinAlgorithm.run(clusters);

### Interpreting the result

Finally, after having constructed the join-expression tree, we need to interpret it over the original BDDs. This requires creating a class that inherits from the JETInterp template class. This subclass will define that joins must be interpreted as conjunctions, projections as existential quantification, and will also specify how to map a leaf node to a BDD according to its identifier. The code is omitted from this example. After having created the interpreter, we use it to evaluate the join-expression tree and obtain the final result as a BDD.

	jet::JETInterp<BDD>* interpreter;

	// Create BDD interpreter here

	BDD result = interpreter->eval(tree);
