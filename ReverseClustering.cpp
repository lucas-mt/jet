#include "ReverseClustering.hpp"

#include <algorithm>

namespace jet
{
ReverseClustering::ReverseClustering(
    std::unique_ptr<Clustering> baseClustering)
    : _baseClustering(std::move(baseClustering))
{}

std::vector<JoinExpTree> ReverseClustering::apply(
    const std::vector<JoinExpTree>& trees) const
{
    std::vector<JoinExpTree> clusters = _baseClustering->apply(trees);

    std::reverse(clusters.begin(), clusters.end());

    return clusters;
}
}
