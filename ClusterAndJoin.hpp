#pragma once

#include <memory>

#include "Clustering.hpp"
#include "JoinExpTree.hpp"
#include "JoinAlgorithm.hpp"

namespace jet
{
/*!
 * \class ClusterAndJoin
 *
 * \brief Combines an existing join algorithm with a clustering step.
 */
class ClusterAndJoin : public JoinAlgorithm
{
    std::unique_ptr<Clustering> _clustering;
    std::unique_ptr<JoinAlgorithm> _joinAlgorithm;

public:

    /*!
     * \brief Constructs a new join algorithm by combining a clustering and
     * an existing join algorithm.
     */
    ClusterAndJoin(std::unique_ptr<Clustering> clustering,
		   std::unique_ptr<JoinAlgorithm> joinAlgorithm);

    /*!
     * \brief Runs the combined algorithm by first applying the clustering and
     * then joining the clusters.
     */
    JoinExpTree run(const std::vector<JoinExpTree>& trees) const;
};
}
