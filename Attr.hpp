#pragma once

#include "Optional.hpp"
#include "Ptr.hpp"

#include <algorithm>
#include <cstddef>
#include <vector>

namespace jet
{
/*!
 * \class Attr
 *
 * \brief Abstract attribute identified by an ID number.
 *
 * Meaning depends on the domain, for example:
 * - a variable in the support of a formula;
 * - an attribute of a database relation;
 * - an index of a tensor.
 */
class Attr
{
  size_t _id;

public:

  /*!
   * \brief Constructs an attribute identified by the given number.
   */
  Attr(size_t id);

  /*!
   * \brief Gets the ID number of the attribute.
   */
  size_t id() const;

  /* Relational operators */
  bool operator==(Attr other) const;
  bool operator!=(Attr other) const;
  bool operator<(Attr other) const;
  bool operator<=(Attr other) const;
  bool operator>(Attr other) const;
  bool operator>=(Attr other) const;

  /*!
   * \brief Constructs a string representation of the attribute.
   *
   * The string is formed by concatenating \a name with the attribute ID.
   */
  std::string toStringWith(std::string name) const;
};

/*!
 * \class AttrSet
 *
 * \brief Abstract representation of an object by a set of attributes.
 *
 * Meaning depends on the domain, for example:
 * - formula represented by its support set;
 * - relation represented by its schema;
 * - tensor represented by its set of indices.
 */
class AttrSet
{
  // Invariants: never null, always sorted, no repeated elements
  ptr<std::vector<Attr>> _attrs;

  // Assumes \a attrs is sorted; does not perform resorting
  AttrSet(ptr<std::vector<Attr>> attrs);

public:

  AttrSet();

  /*!
   * \brief Constructs a set containing the attributes in the range.
   *
   * The range represented by \a first and \a last does not need to be sorted
   * and can contain repetitions.
   */
  template<class Iterator>
  AttrSet(Iterator first, Iterator last)
  {
    _attrs = std::make_shared<std::vector<Attr>>(first, last);

    std::sort(_attrs->begin(), _attrs->end());

    // Remove duplicates
    std::vector<Attr>::iterator newEnd =
      std::unique(_attrs->begin(), _attrs->end());
    _attrs->resize(std::distance(_attrs->begin(), newEnd), Attr(0));
  }
  
  
  /*!
   * \brief Constructs a set from a vector of attributes.
   *
   * The vector \a attrs can be unsorted and contain repetitions.
   */
  AttrSet(std::vector<Attr> attrs);

  /*!
   * \brief Constructs a set with a single element.
   */
  static AttrSet singleton(Attr attr);

  /*!
   * \brief Returns the number of elements in the set.
   */
  size_t size() const;

  /*!
   * \brief Takes the union with another set of attributes.
   */
  AttrSet unionWith(const AttrSet& other) const;

  /*!
   * \brief Takes the intersection with another set of attributes.
   */
  AttrSet intersectWith(const AttrSet& other) const;

  /*!
   * \brief Takes the difference with another set of attributes.
   */
  AttrSet differenceWith(const AttrSet& other) const;

  /*!
   * \brief Takes the union of all sets in \a args.
   */
  static AttrSet bigUnion(const std::vector<AttrSet>& args);

  /*!
   * \brief Tests if attribute is in the set.
   */
  bool hasElem(Attr attr) const;

  /*!
   * \brief Tests if this is a subset of the given set.
   */
  bool subsetOf(const AttrSet& other) const;

  /*!
   * \brief Returns maximum element of the set, if one exists.
   */
  optional<Attr> maxElem() const;

  /* Relational operators */
  bool operator==(const AttrSet& other) const;
  bool operator!=(const AttrSet& other) const;

  /* Iterators */
  typedef typename std::vector<Attr>::const_iterator const_iterator;

  inline const_iterator begin() const { return _attrs->begin(); }
  inline const_iterator end() const { return _attrs->end(); }

  /*!
   * \brief Constructs a string representation of the set.
   *
   * The argument \a name is used as a prefix for the attributes.
   */
  std::string toStringWith(std::string name) const;
};
}

/*!
 * \brief Specialization of hash template to Attr class.
 */
namespace std
{
    template<>
    struct hash<jet::Attr>
    {
	size_t operator()(const jet::Attr& attr) const
	{
	    return hash<size_t>()(attr.id());
	}
    };
}
