#pragma once

#include "JoinExpTree.hpp"
#include "JETFunction.hpp"

#include <vector>

namespace jet
{
/*!
 * \class JETInterp
 *
 * \brief Interpreter for a join expression tree on a specific domain.
 *
 * Interprets the join expression tree as a sequence of joins and projections
 * over elements of type T.
 */
template<class T>
class JETInterp : public JETFunction<T>
{
public:

  /*!
   * \brief Interpretation for the empty node.
   */
  virtual T emptyInterp() = 0;

  /*!
   * \brief Interpretation for the leaf node.
   */
  virtual T leafInterp(size_t id) = 0;

  /*!
   * \brief Interpretation for the join node.
   */
  virtual T joinInterp(const std::vector<T>& args) = 0;

  /*!
   * \brief Interpretation for the projection node.
   */
  virtual T projInterp(const AttrSet& projAttrs, const T& arg) = 0;

  /*!
   * \brief Stores the result for an empty node.
   */
  void forEmptyNode(const EmptyNode& emptyNode)
  {
    this->_result = this->emptyInterp();
  }

  /*!
   * \brief Stores the result for a leaf node.
   */
  void forLeafNode(const LeafNode& leafNode)
  {
    this->_result = this->leafInterp(leafNode.id());
  }

  /*!
   * \brief Stores the result for a join node.
   */
  void forJoinNode(const JoinNode& joinNode)
  {
    std::vector<T> args;

    args.reserve(joinNode.childCount());

    joinNode.forEachChild([this, &args] (JoinExpTree child)
			  {
			    args.push_back(this->eval(child));
			  });

    this->_result = this->joinInterp(args);
  }

  /*!
   * \brief Stores the result for a projection node.
   */
  void forProjNode(const ProjNode& projNode)
  {
    this->_result = this->projInterp(projNode.projectedAttrs(),
				     this->eval(projNode.child()));
  }

  /*!
   * \brief
   */
  void forListNode(const ListNode& listNode)
  {
    T result = this->emptyInterp();

    listNode.forEachStep([this, &result] (const LinearStep& step)
			 {
			   T toJoin = this->eval(step.toJoin());
			   result = this->joinInterp({ result, toJoin });

			   AttrSet toProject = step.toProject();
			   result = this->projInterp(toProject, result);
			 });

    this->_result = result;
  }
  
  /* Virtual destructor */
  virtual ~JETInterp() {}
};
}
