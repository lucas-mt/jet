#pragma once

#include "Attr.hpp"

namespace jet
{
// Forward declarations
class JETNode;
class LinearStep;
class JETVisitor;

/*!
 * \class JoinExpTree
 *
 * \brief Join-expression tree describing the evaluation order of a join expression.
 *
 * A join-expression tree is a tree where every node is labeled by a set of attributes,
 * and for every attribute all nodes containing this attribute form a subtree.
 */
class JoinExpTree
{
  // Invariant: never null
  ptr<JETNode> _node;

  JoinExpTree(ptr<JETNode> node);

public:

  /*!
   * \brief Constructs a node with no children labeled by the empty set.
   */
  static JoinExpTree empty();

  /*!
   * \brief Constructs a leaf of the tree labeled by the given set of attributes.
   */
  static JoinExpTree leaf(size_t id, AttrSet label);

  /*!
   * \brief Constructs a join-expression tree by joining the given subtrees.
   */
  static JoinExpTree join(std::vector<JoinExpTree> children);

  /*!
   * \brief Projects the given attributes from the root of the tree.
   */
  JoinExpTree proj(AttrSet toProject) const;

  /*!
   * \brief
   */
  static JoinExpTree list(std::vector<LinearStep> steps);

  /*!
   * \brief Joins this join-expression tree with one other tree.
   */
  JoinExpTree operator*(JoinExpTree other) const;

  /*!
   * \brief Gets the set of attributes serving as label to the tree.
   */
  AttrSet label() const;

  void accept(JETVisitor& visitor) const;
};

/*!
 * \class JETNode
 *
 * \brief Node of a join-expression tree.
 */
class JETNode
{
protected:

    AttrSet _label;

public:

    /*!
     * \brief Constructs a node with the given label.
     */
    JETNode(AttrSet label);

    /*!
     * \brief Returns the label of this node.
     */
    AttrSet label() const;

    /*!
     * \brief Runs the given visitor on this node.
     */
    virtual void accept(JETVisitor& visitor) const = 0;

    /* Virtual destructor */
    virtual ~JETNode() {}
};

/*!
 * \class EmptyNode
 *
 * \brief Node with an empty label and no children.
 */
class EmptyNode : public JETNode
{
public:

    /*!
     * \brief Constructs an empty node.
     */
    EmptyNode();

    void accept(JETVisitor& visitor) const;
};

/*!
 * \class LeafNode
 *
 * \brief Leaf node representing a domain object.
 *
 * Other than the label, a leaf node contains a user-assigned ID
 * corresponding to the domain object it represents.
 */
class LeafNode : public JETNode
{
    size_t _id;

public:

    /*!
     * \brief Constructs a leaf node with the given ID and label.
     */
    LeafNode(size_t id, AttrSet label);

    /*!
     * \brief Returns the ID of this node.
     */
    size_t id() const;

    void accept(JETVisitor& visitor) const;
};

/*!
 * \class JoinNode
 *
 * \brief Node representing the join of multiple subtrees.
 */
class JoinNode : public JETNode
{
    std::vector<JoinExpTree> _children;

public:

    /*!
     * \brief Constructs a node joining the given children.
     *
     * The label of the node will be the union of its children's labels.
     */
    JoinNode(std::vector<JoinExpTree> children);

    /*!
     * \brief Executes the given action for each of the node's children.
     *
     * The argument is assumed to define operator().
     */
    template<class F>
    void forEachChild(const F& action) const
    {
        for (JoinExpTree child : _children)
        {
	    action(child);
        }
    }


    /*!
     * \brief Returns the number of children of the node.
     */
    size_t childCount() const;

    /*!
     * \brief Returns the i-th child of the node.
     */
    JoinExpTree getChild(size_t i) const;

    void accept(JETVisitor& visitor) const;
};

/*!
 * \class ProjNode
 *
 * \brief Node representing a projection.
 *
 */
class ProjNode : public JETNode
{
    AttrSet _projAttrs;

    JoinExpTree _child;

public:

    /*!
     * \brief Constructs a node given a child and set of attributes to project.
     *
     * The label of the node will be the child's label with the given attributes
     * removed.
     */
    ProjNode(AttrSet projAttrs, JoinExpTree child);

    /*!
     * \brief Returns the set of attributes projected at this node.
     */
    AttrSet projectedAttrs() const;

    /*!
     * \brief Returns this node's child.
     */
    JoinExpTree child() const;

    void accept(JETVisitor& visitor) const;
};

/*!                                                                            
 * \class LinearStep                                  
 *
 * \brief Individual step to be performed in a linear scan, composed of a tree
 * to be joined and a set of attributes to be projected.                    
 */
class LinearStep
{
  JoinExpTree _tree;
  AttrSet _attrs;

public:

  /*!                                                                     
   * \brief Constructs a step given the joined tree and projected attributes.
   */
  LinearStep(JoinExpTree tree, AttrSet attrs);

  /*!                                                                     
   * \brief Returns the join-expression tree to be joined in this step.   
   */
  JoinExpTree toJoin() const;

  /*!                                                                     
   * \brief Returns the set of attributes to be projected after the join.
   */
  AttrSet toProject() const;
};
  
/*!
 * \class ListNode
 *
 * \brief
 */
class ListNode : public JETNode
{
  std::vector<LinearStep> _steps;

  static AttrSet remainingAttrs(const std::vector<LinearStep>& steps);
  
public:

  ListNode(std::vector<LinearStep> steps);

  template<class F>
  void forEachStep(const F& action) const
  {
    for (const LinearStep& step : _steps)
    {
      action(step);
    }
  }

  void accept(JETVisitor& visitor) const;
};

  
/*!
 * \class JETVisitor
 *
 * \brief Visitor class for traversing a JoinExpressionTree.
 */
class JETVisitor
{
public:

  /*!
   * \brief Method called when the visitor visits an empty node.
   */
  virtual void forEmptyNode(const EmptyNode& emptyNode) = 0;

  /*!
   * \brief Method called when the visitor visits a leaf node.
   */
  virtual void forLeafNode(const LeafNode& leafNode) = 0;

  /*!
   * \brief Method called when the visitor visits a join node.
   */
  virtual void forJoinNode(const JoinNode& joinNode) = 0;

  /*!
   * \brief Method called when the visitor visits a projection node.
   */
  virtual void forProjNode(const ProjNode& projNode) = 0;

  /*!
   * \brief
   */
  virtual void forListNode(const ListNode& listNode) = 0;

  /* Virtual destructor */
  virtual ~JETVisitor() {}
};
}
