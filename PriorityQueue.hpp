#pragma once

#include <queue>
#include <unordered_map>

namespace jet
{
namespace data
{
    /*!
     * \class WithPriority
     *
     * \brief Record class associating an element with a priority.
     */
    template<class T, class P>
    class WithPriority
    {
	std::pair<P, T> _pair;

    public:

	/*!
	 * \brief Creates an object associating a value with a priority.
	 */
	WithPriority(T value, P priority)
	    : _pair(std::move(priority), std::move(value))
	{}

	/*!
	 * \brief Returns the value of this object.
	 */
	T value() const
	{
	    return _pair.second;
	}

	/*!
	 * \brief Returns the priority of this object.
	 */
	P priority() const
	{
	    return _pair.first;
	}

	/*!
	 * \brief Compares two values with priority.
	 *
	 * Evaluates to true either if the second operand has a higher
	 * priority, or if both operands have the same priority and the
	 * second operand has a higher value.
	 */
	bool operator<(const WithPriority<T, P>& other) const
	{
	    return _pair < other._pair;
	}
    };

    /*!
     * \class PriorityQueue
     *
     * \brief Max priority queue data structure supporting update to the
     * priority of elements.
     */
    template<class T, class P>
    class PriorityQueue
    {
	std::unordered_map<T, P> _priority;
	std::priority_queue<WithPriority<T, P>> _queue;

    public:

	/*!
	 * \brief Constructs a priority queue and adds the elements
	 * with priority in the given range.
	 */
	template<class It>
	PriorityQueue(It begin, It end)
	    : _queue(begin, end)
	{
	    for (auto it = begin; it != end; it++)
	    {
		_priority[it->value()] = it->priority();
	    }
	}

	/*!
	 * \brief Pops and returns the element with higher priority,
	 * or nothing is the queue is empty.
	 */
	optional<T> pop()
	{
	    while (!_queue.empty())
	    {
		WithPriority<T, P> elem = _queue.top();
		_queue.pop();

		T value = elem.value();
		auto it = _priority.find(value);

		if (it != _priority.end() && it->second == elem.priority())
		{
		    _priority.erase(it);

		    return value;
		}
	    }

	    return nullopt;
	}

	/*!
	 * \brief Updates the priority of the given element using the
	 * provided update function.
	 */
	template<class F>
	void updatePriority(const T& elem, const F& update)
	{
	    auto it = _priority.find(elem);

	    if (it != _priority.end())
	    {
		_priority[elem] = update(it->second);

		_queue.emplace(elem, it->second);
	    }
	}
    };
}
}
