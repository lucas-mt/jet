#pragma once

#include "JoinExpTree.hpp"

#include <vector>

namespace jet
{
/*!
 * \class Clustering
 *
 * \brief Abstract class representing a clustering strategy.
 */
class Clustering
{
public:

	/*!
	 * \brief Join trees into clusters according to the clustering algorithm.
	 */
	virtual std::vector<JoinExpTree> apply(const std::vector<JoinExpTree>& trees) const = 0;

	/*!
	 * \brief Virtual destructor.
	 */
	virtual ~Clustering() {}
};
}
