#pragma once

#include <experimental/optional>

namespace jet
{
template<class T>
using optional = std::experimental::optional<T>;

using nullopt_t = std::experimental::nullopt_t;

constexpr nullopt_t nullopt = std::experimental::nullopt;
}
