#include "LinearScan.hpp"

#include <memory>

namespace jet
{
LinearScheduler::~LinearScheduler() {}

LinearScan::LinearScan(AttrSet projectedAttrs)
  : _projectedAttrs(std::move(projectedAttrs))
{}

JoinExpTree LinearScan::run(const std::vector<JoinExpTree>& trees) const
{
  ptr<LinearScheduler> scheduler = this->makeScheduler(_projectedAttrs, trees);

  std::vector<LinearStep> steps;

  while (true)
  {
    optional<LinearStep> nextStep = scheduler->nextStep();

    if (!nextStep)
      break;
    else
      steps.push_back(*nextStep);
  }

  return JoinExpTree::list(std::move(steps));
}

LinearScan::~LinearScan() {}
}
