SRCS=Attr.cpp AttrRanking.cpp BucketElimination.cpp ClusterAndJoin.cpp Clustering.hpp DOTInterp.cpp GreedyScan.cpp InOrderScan.cpp InOrderSet.hpp JETFunction.hpp JETInterp.hpp JoinAlgorithm.hpp JoinExpTree.cpp LinearScan.cpp Optional.hpp PrimalGraph.cpp PriorityQueue.hpp Ptr.hpp ReverseClustering.cpp
OBJS=$(subst .cpp,.o,$(SRCS))

lib: $(OBJS)
	mkdir -p lib && ar rcs lib/libjet.a $(OBJS)

%.o: %.c
	g++ -std=c++14 -c $<

clean:
	rm -f *.o lib/libjet.a
