#pragma once

#include "LinearScan.hpp"

namespace jet
{
/*!
 * \class InOrderScheduler
 *
 * \brief Schedules the trees in the order they are given.
 */
class InOrderScheduler : public LinearScheduler
{
	std::vector<LinearStep> _steps;

public:

	/*!
	 * \brief Constructs the scheduler for the given trees and attributes to project.
	 */
	InOrderScheduler(const AttrSet& projectedAttrs,
	                 const std::vector<JoinExpTree>& trees);

	/*!
	 * \brief Returns the next step in the join.
	 */
	optional<LinearStep> nextStep();
};

/*!
 * \class InOrderScan
 *
 * \brief Linear scan that joins the trees in the order they are given.
 */
class InOrderScan : public LinearScan
{
protected:

	/*!
	 * \brief Factory method for constructing an in-order scheduler.
	 */
	ptr<LinearScheduler> makeScheduler(const AttrSet& projectedAttrs,
	                                   const std::vector<JoinExpTree>& trees) const;

public:

	/*!
	 * \brief Constructs an in-order scan algorithm given the attributes to project.
	 */
	InOrderScan(AttrSet projectedAttrs);
};
}
