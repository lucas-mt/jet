#include "GreedyScan.hpp"

#include <memory>

namespace jet
{
GreedyScheduler::GreedyScheduler(const AttrSet& projectedAttrs,
                                 const std::vector<JoinExpTree>& trees)
  : _trees(trees)
{
  for (size_t i = 0; i < _trees.size(); i++)
  {
    for (Attr attr : _trees[i].label())
    {
      if (projectedAttrs.hasElem(attr))
      {
	_projectedSupport[i].insert(attr);
	_memberRelation[attr].insert(i);
      }
    }
  }
}

std::unordered_set<Attr>
GreedyScheduler::uniqueAttrs(const std::unordered_set<Attr>& attrs) const
{
  std::unordered_set<Attr> uniqueAttrs;
  
  for (Attr attr : attrs)
  {
    if (_memberRelation.at(attr).size() == 1)
    {
      uniqueAttrs.insert(attr);
    }
  }
  
  return uniqueAttrs;
}

void GreedyScheduler::remove(size_t i)
{
  for (Attr attr : _projectedSupport[i])
  {
    _memberRelation[attr].erase(i);

    if (_memberRelation[attr].empty())
    {
      _memberRelation.erase(attr);
    }
  }
  
  _projectedSupport.erase(i);
}

optional<LinearStep> GreedyScheduler::nextStep()
{
  if (_memberRelation.empty())
  {
    return nullopt;
  }
  else
  {
    size_t bestIndex = 0;
    std::unordered_set<Attr> bestAttrs;

    for (const auto& entry : _projectedSupport)
    {
      size_t i = entry.first;
      
      std::unordered_set<Attr> uniqueAttrs = this->uniqueAttrs(entry.second);

      if (uniqueAttrs.size() >= bestAttrs.size())
      {
	bestIndex = i;
	bestAttrs = uniqueAttrs;
      }
    }

    this->remove(bestIndex);
    
    JoinExpTree nextTree = _trees[bestIndex];
    AttrSet nextProjected = AttrSet(bestAttrs.begin(), bestAttrs.end());
    
    return LinearStep(std::move(nextTree), std::move(nextProjected));
  }
}

GreedyScan::GreedyScan(AttrSet projectedAttrs)
  : LinearScan(std::move(projectedAttrs))
{}

ptr<LinearScheduler>
GreedyScan::makeScheduler(const AttrSet& projectedAttrs,
			  const std::vector<JoinExpTree>& trees) const
{
  return std::make_shared<GreedyScheduler>(projectedAttrs, trees);
}
}
