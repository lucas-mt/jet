#pragma once

#include <memory>

#include "Clustering.hpp"
#include "JoinExpTree.hpp"

namespace jet
{
/*!
 * \class ReverseClustering
 *
 * \brief Clustering strategy that reverses an existing clustering.
 */
class ReverseClustering : public Clustering
{
    std::unique_ptr<Clustering> _baseClustering;

public:

    /*!
     * \brief Constructs a clustering by reversing \a baseClustering.
     */
    ReverseClustering(std::unique_ptr<Clustering> baseClustering);

    /*!
     * \brief Returns the same clusters as the base clustering, but on the
     * reverse order.
     */
    std::vector<JoinExpTree> apply(const std::vector<JoinExpTree>& trees) const;
};
}
