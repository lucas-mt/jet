#include "InOrderScan.hpp"

namespace jet
{
InOrderScheduler::InOrderScheduler(const AttrSet& projectedAttrs,
                                   const std::vector<JoinExpTree>& trees)
{
	_steps.reserve(trees.size());

	AttrSet remaining;

	for (auto it = trees.rbegin(); it != trees.rend(); it++)
	{
		AttrSet toProject =
			it->label()
	        .intersectWith(projectedAttrs)
	        .differenceWith(remaining);

		remaining = remaining.unionWith(toProject);

		_steps.emplace_back(*it, std::move(toProject));
	}
}

optional<LinearStep> InOrderScheduler::nextStep()
{
	if (!_steps.empty())
	{
		LinearStep nextStep = _steps.back();

		_steps.pop_back();

		return nextStep;
	}
	else
	{
		return nullopt;
	}
}

InOrderScan::InOrderScan(AttrSet projectedAttrs)
	: LinearScan(std::move(projectedAttrs))
{}

ptr<LinearScheduler> InOrderScan::makeScheduler(
	const AttrSet& projectedAttrs,
	const std::vector<JoinExpTree>& trees) const
{
	return std::make_shared<InOrderScheduler>(projectedAttrs, trees);
}
}
