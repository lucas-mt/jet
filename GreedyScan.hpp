#pragma once

#include "LinearScan.hpp"
#include "JoinExpTree.hpp"
#include "Attr.hpp"

#include <vector>
#include <unordered_set>
#include <unordered_map>

namespace jet
{
/*!
 * \class GreedyScheduler
 *
 * \brief Chooses at every step to join the tree that will cause the largest
 * number of attributes to be projected out.
 */
class GreedyScheduler : public LinearScheduler
{
  std::vector<JoinExpTree> _trees;
  std::unordered_map<size_t, std::unordered_set<Attr>> _projectedSupport;
  std::unordered_map<Attr, std::unordered_set<size_t>> _memberRelation;

  std::unordered_set<Attr>
  uniqueAttrs(const std::unordered_set<Attr>& attrs) const;

  void remove(size_t i);

public:

	/*!
	 * \brief Constructs the scheduler for the given trees and attributes to project.
	 */
	GreedyScheduler(const AttrSet& projectedAttrs,
	                const std::vector<JoinExpTree>& trees);

	/*!
	 * \brief Returns the next step in the join.
	 */
	optional<LinearStep> nextStep();
};

/*!
 * \class GreedyScan
 *
 * \brief Linear scan that uses a greedy approach to choose the next step.
 */
class GreedyScan : public LinearScan
{
protected:

	/*!
	 * \brief Factory method for constructing a greedy scheduler.
	 */
	ptr<LinearScheduler> makeScheduler(const AttrSet& projectedAttrs,
	                                   const std::vector<JoinExpTree>& trees) const;

public:

	/*!
	 * \brief Constructs a greedy scan algorithm given the attributes to project
	 */
	GreedyScan(AttrSet projectedAttrs);
};
}
